#!/bin/python3
#https://www.hackerrank.com/challenges/swap-nodes-algo/problem
import os
import sys
import math

# nice hack!
sys.setrecursionlimit(1500)

class TNode:
    def __init__(self, v, l, r):
        self.v = v
        self.l = l
        self.r = r

# given a complete tree converts the array tree representation in a pointers
# based representation
def create_binary_tree(indexes):
    complete_tree = [1] + indexes
    n = len(complete_tree)

    root = TNode(complete_tree[0], None, None)
    all_nodes = [root]
    i = 0
    leaf_count = 0
    while i < n:
        current_node = all_nodes[i]

        if complete_tree[i] == -1:
            leaf_count = leaf_count + 1
        else:
            li = (2 * i + 1) - 2 * leaf_count
            ri = (2 * i + 2) - 2 * leaf_count

            l = None
            r = None

            l = TNode(complete_tree[li], None, None)
            r = TNode(complete_tree[ri], None, None)

            all_nodes.append(l)
            all_nodes.append(r)

            current_node.l = l
            current_node.r = r

        i = i + 1
    return root

# performs swaps at level _depth % k == 0 for root tree
def swap_at_nk(root, k, _depth = 1):
    if root is None:
        return

    should_swap = _depth % k == 0

    if should_swap:
        tmp = root.l
        root.l = root.r
        root.r = tmp

    swap_at_nk(root.l, k, _depth + 1)
    swap_at_nk(root.r, k, _depth + 1)

# traverses tree by given root and returns stored in order traversal array
# Python's default recursion limit is 1000, meaning that Python won't
# let a function call on itself more than 1000 times, which
# for most people is probably enough. The limit exists because
# allowing recursion to occur more than 1000 times doesn't
# exactly make for lightweight code.Oct 10, 2016
def in_order_traversal_rec(root):
    if root is None:
        return []

    traversal = []
    left = in_order_traversal_rec(root.l)
    if root.v > 0:
        traversal.append(root.v)
    right = in_order_traversal_rec(root.r)

    return left + traversal + right

def in_order_traversal(root):
    c = root
    s = []
    v = []
    done = False

    while not done:
        if c is not None:
            s.append(c)
            c = c.l
        else:
            if len(s) > 0:
                c = s.pop()
                if c.v != -1:
                    v.append(c.v)
                c = c.r
            else:
                done = True
    return v

# main swap call
def swap_nodes(indexes, queries):
    unfolded_indexes = []

    for index in indexes:
        unfolded_indexes.append(index[0])
        unfolded_indexes.append(index[1])

    root = create_binary_tree(unfolded_indexes)
    results = []

    for k in queries:
        swap_at_nk(root, k)
        results.append(in_order_traversal(root))

    return results

# input-2 out
# [2, 9, 6, 4, 1, 3, 7, 5, 11, 8, 10] | 11
# [2, 6, 9, 4, 1, 3, 7, 5, 10, 8, 11] | 11
if __name__ == '__main__':
    filename = './medium/swap-nodes-algo/input-2.txt'

    with open(filename) as file_object:
        lines = file_object.readlines()

    n = int(lines[0])

    indexes = []

    for line in lines[1:n + 1]:
        indexes.append(list(map(int, line.rstrip().split())))

    queries_count = int(lines[n + 2])

    queries = []

    for line in lines[n + 2:]:
        queries_item = int(line)
        queries.append(queries_item)

    results = swap_nodes(indexes, queries)

    print('\n'.join([' '.join(map(str, x)) for x in results]))
