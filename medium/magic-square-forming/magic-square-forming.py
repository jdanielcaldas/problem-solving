#!/bin/python3
#https://www.hackerrank.com/challenges/magic-square-forming/problem
import math
import os
import random
import re
import sys

# there are a finite number of combinations
# to form a 3x3 magic square, taking advantage of it
# helps us to solve the challenge
magic_squares = [
    [[4, 9, 2], [3, 5, 7], [8, 1, 6]],
    [[8, 3, 4], [1, 5, 9], [6, 7, 2]],
    [[2, 7, 6], [9, 5, 1], [4, 3, 8]],
    [[6, 1, 8], [7, 5, 3], [2, 9, 4]],
    [[2, 9, 4], [7, 5, 3], [6, 1, 8]],
    [[6, 7, 2], [1, 5, 9], [8, 3, 4]],
    [[8, 1, 6], [3, 5, 7], [4, 9, 2]],
    [[4, 3, 8], [9, 5, 1], [2, 7, 6]],
]

def forming_magic_square(s):
    n = len(s)
    n_magic_squares = len(magic_squares)
    # store cost of transforming s into magic_square[i] initialize to 0
    costs = [x - x for x in range(n_magic_squares)]

    i = 0
    while i < n:
        j = 0
        while j < n:
            mi = 0
            while mi < n_magic_squares:
                magic_square = magic_squares[mi]
                costs[mi] = costs[mi] + abs(magic_square[i][j] - s[i][j])
                mi = mi + 1
            j = j + 1
        i = i + 1

    return min(costs)


if __name__ == '__main__':
    filename = './medium/magic-square-forming/input-3.txt'

    s = []

    with open(filename) as file_object:
        lines = file_object.readlines()

    for line in lines:
        s.append(list(map(int, line.rstrip().split())))

    result = forming_magic_square(s)

    print(f"Cost: {result}")