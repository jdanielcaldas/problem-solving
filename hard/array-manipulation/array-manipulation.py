
#!/bin/python3
#https://www.hackerrank.com/challenges/crush/problem
import math
import os
import random
import re
import sys

# sort by key
# def q_key(q):
#     return q[2]
# cut_queries = sorted(queries, key=q_key, reverse=True)[0:cut]

def array_manipulation(n, queries):
    final_array = [x - x for x in range(n + 1)]

    for q in queries:
        [a,b,k] = q
        final_array[a] = final_array[a] + k
        if (b + 1) <= n:
            final_array[b + 1] = final_array[b + 1] - k

    i = 1
    x = 0
    max_value = 0
    while i <= n:
        x = x + final_array[i]
        if max_value < x:
            max_value = x
        i = i + 1

    return max_value

if __name__ == '__main__':
    filename = './hard/array-manipulation/input-3.txt'

    with open(filename) as file_object:
        lines = file_object.readlines()

    nm = lines[0].split()
    n = int(nm[0])
    m = int(nm[1])

    queries = []

    for line in lines[1:]:
        queries.append(list(map(int, line.rstrip().split())))

    result = array_manipulation(n, queries)

    print(result)
