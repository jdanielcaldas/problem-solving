#!/bin/python3
#https://www.hackerrank.com/challenges/camelcase/problem?utm_campaign=challenge-recommendation&utm_medium=email&utm_source=24-hour-campaign
import math
import os
import random
import re
import sys

def camelcase(s):
    return len(list(filter(lambda c: c.isupper(), s))) + 1

if __name__ == '__main__':
    filename = './easy/camel-case/input-1.txt'

    with open(filename) as file_object:
        lines = file_object.readlines()

    result = camelcase(lines[0])

    print(result)
