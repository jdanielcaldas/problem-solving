#!/bin/python
#https://www.hackerrank.com/challenges/2d-array/problem
import math
import os
import random
import re
import sys
import subprocess

def get_hour_glass_sum(arr, i, j):
    positions = [
        [i, j],
        [i, j + 1],
        [i, j + 2],
        [i + 1, j + 1],
        [i + 2, j],
        [i + 2, j + 1],
        [i + 2, j + 2]
    ]

    # TODO: from line 21 to 33 there is certainly a clear way to achieve this
    for position in positions:
        [i, j] = position
        try:
           x = arr[i][j]
        except:
            return None

    sum = 0
    for position in positions:
        [i, j] = position
        sum = sum + arr[i][j]

    return sum

def hourglass_sum(arr):
    max_hour_glass_sum = -63
    ni = len(arr)

    i = 0
    while i < ni:
        r = arr[i]
        nj = len(r)
        j = 0

        while j < nj:
            hourglass_sum = get_hour_glass_sum(arr, i, j)
            if hourglass_sum is not None:
                # TODO: maybe such long expression is not ideal, breakdown conditional
                max_hour_glass_sum = hourglass_sum if hourglass_sum > max_hour_glass_sum else max_hour_glass_sum
            j = j + 1

        i = i + 1

    return max_hour_glass_sum

if __name__ == '__main__':
    filename = './easy/2d-array-ds/input-1.txt'

    # exec shell commands within python
    # out = subprocess.run(["ls", "-l"])
    # print(out)

    with open(filename) as file_object:
        lines = file_object.readlines()

    arr = []

    for i in range(1, 6):
        arr.append(list(map(int, lines[i].rstrip().split())))

    result = hourglass_sum(arr)

    print(result)
