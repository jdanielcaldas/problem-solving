#!/bin/python3
#https://www.hackerrank.com/challenges/dynamic-array/problem
import math
import os
import random
import re
import sys

def dynamic_array(n, queries):
    seqList = [[] for x in range(n)]
    last_answer = 0
    results = []

    for q in queries:
        [x, y] = q[1:]
        if q[0] == 1:
            i = (x ^ last_answer) % n
            seqList[i].append(y)
        elif q[0] == 2:
            # TODO: too many variables declared, revisit
            seqi = (x ^ last_answer) % n
            seq = seqList[seqi]
            size = len(seq)
            if size == 0:
                i = 0
            else:
                i = y % size
            if seq != -1:
                last_answer = seq[i]
                results.append(last_answer)

    return results

if __name__ == '__main__':
    filename = './easy/dynamic-array/input-2.txt'

    with open(filename) as file_object:
        lines = file_object.readlines()

    nq = lines[0].rstrip().split()

    n = int(nq[0])

    q = int(nq[1])

    queries = []

    _lines = lines[1:]

    for l in _lines:
        queries.append(list(map(int, l.rstrip().split())))

    results = dynamic_array(n, queries)

    print(results)
